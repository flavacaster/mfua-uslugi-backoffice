import {useCallback, useEffect, useRef, useState} from "react";
import OrdersTable from "./OrdersTable";
import {useDispatch, useSelector} from "react-redux";
import {Spinner} from "react-bootstrap";
import ResultModal from "./ResultModal";

export default function Orders() {
    const [orders, setOrders] = useState([]);
    const [modalActive, setModalActive] = useState(false);
    const {modalData} = useSelector(state => state.reducer);
    const {statusEnums, locale} = useSelector(state => state.reducer);
    const ordersContainer = useRef(null);
    const modalContainer = useRef(null);
    const dispatch = useDispatch();

    const getCatalog = useCallback(() => {
        let res =  window.axios.get(`/manager/v1/catalog`)
            .then(res => {
                if (!res.data.data) return;
                setOrders(res.data.data);
            });
    });

    useEffect(() => {
        getCatalog()
    }, []);


    return (
        <div className='orders'>
            <div className="orders__header">{window.L10N.get('orders.title')}</div>
            <div className="orders__content" ref={ordersContainer}>
                {
                    orders && orders.length ? <OrdersTable modalShow={setModalActive} locale={locale} dispatch={dispatch} orders={orders} statusEnums={statusEnums}/>
                        : <Spinner variant={"info"} animation={"border"} style={{width: '10rem', height: '10rem'}}/>
                }
            </div>
            <ResultModal show={modalActive} setShow={setModalActive} content={modalData.content} type={modalData.type}/>
        </div>
    )
}
