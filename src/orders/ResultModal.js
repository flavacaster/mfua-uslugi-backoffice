import {Button, Modal} from "react-bootstrap";

export default function ResultModal({type, content, show, setShow}) {
    const handleClose = () => setShow(false);
    let modalContent = [];
    if (content) {
        for (let key in content) {
            modalContent.push(content[key]);
        }

    }

    return (
        <Modal show={show} centered autoFocus onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>{window.L10N.get(`orders.${type}`)}</Modal.Title>
            </Modal.Header>
            {
                modalContent.length ? <Modal.Body>{modalContent.map((el, idx) => <div key={idx} className='error-text-row'>{el}</div>)}</Modal.Body> : null
            }
            <Modal.Footer>
                <Button onClick={handleClose}>{window.L10N.get('modal.close')}</Button>
            </Modal.Footer>
        </Modal>
    )
}
