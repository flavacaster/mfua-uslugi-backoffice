import React, {useRef, useState} from "react";
import {Table, Button} from "react-bootstrap";
import moment from "moment";
import 'moment/locale/ru';
import * as Actions from "../store/actions";

export default function OrdersTable({modalShow, orders, statusEnums, dispatch, locale}) {
    const [isFilePicked, setIsFilePicked] = useState(false);
    const btnRefs = useRef(orders.map(() => React.createRef()));

    const getBase64 = file => {
        return new Promise(resolve => {
            let fileInfo;
            let baseURL = "";
            // Make new FileReader
            let reader = new FileReader();

            // Convert the file to base64 text
            reader.readAsDataURL(file);

            // on reader load somthing...
            reader.onload = () => {
                // Make a fileInfo Object
                baseURL = reader.result;
                resolve(baseURL);
            };
        });
    };
    const setMomentLocale = () => {
        if (locale) {
            const l = locale.split('-')[0];
            moment.locale(l);
        }
    };

    const changeHandler = async (event) => {
        dispatch(Actions.toggleSpinner(true));
        const id = event.target.id;
        const btn = btnRefs.current.find(b => b.getAttribute('data-id') === id);
        const documentContent = await getBase64(event.target.files[0]).then(res => res);
        setIsFilePicked(true);

        await window.axios.post(`/manager/v1/catalog/upload?id=${id}`, {document: documentContent})
            .then(() => {
                btn.innerText = event.target.files[0].name;
                btn.className = 'btn btn-success';
                dispatch(Actions.setModalData({type: 'success'}));
                modalShow(true);
                dispatch(Actions.toggleSpinner(false));
            }).catch(err => {
                const message = err.response.data;
                dispatch(Actions.setModalData({type: 'error', content: message.data}));
                modalShow(true);
                dispatch(Actions.toggleSpinner(false));
            });
    };

    const renderTableHeaders = () => {
        const fields = Object.keys(orders[0] || []);
        fields[5] = ' ';
        return <HeaderRow fields={fields}/>;
    };

    const renderTableContent = () => {
        setMomentLocale();
        return orders.map((o, idx) => {
            if (typeof o.dateOrdered !== 'string') {
                o.dateOrdered = moment(o.dateOrdered).format("DD MM YYYY HH:mm");
            }
            if (o.status && typeof o.status === 'number') {
                const statusNumber = o.status
                o.status = window.L10N.get(`orders.table.orderStatus.${statusEnums[`${statusNumber}`]}`);
            }
            return <BodyRow
                changeHandler={changeHandler}
                handleApprove={handleApprove}
                handleDecline={handleDecline}
                key={idx}
                idx={idx}
                data={o}
                changeFile={changeHandler}
                handleUpload={handleUpload}
                isFilePicked={isFilePicked}
                btnRefs={btnRefs}
            />;
        });
    }

    const handleUpload = async (e) => {
        try {
            const id = e.target.getAttribute('data-id');
            const input = document.getElementById(`${id}`);
            input?.click();
        } catch (e) {
            console.error(e);
        }
    }

    const handleApprove = async (e) => {
        dispatch(Actions.toggleSpinner(true));
        const id = e.target.getAttribute('data-id');

        await window.axios.post(`/manager/v1/catalog/approve?id=${id}`)
            .then(() => {
                dispatch(Actions.toggleSpinner(false));
                dispatch(Actions.setModalData({type: 'success'}));
                modalShow(true);
            }).catch(err => {
            const message = err.response.data;
            dispatch(Actions.setModalData({type: 'error', content: message.data}));
            modalShow(true);
            dispatch(Actions.toggleSpinner(false));
        });
    }

    const handleDecline = async (e) => {
        dispatch(Actions.toggleSpinner(true));

        const id = e.target.getAttribute('data-id');

        await window.axios.post(`/manager/v1/catalog/decline?id=${id}`)
            .then(() => {
                dispatch(Actions.setModalData({type: 'success'}));
                modalShow(true);
                dispatch(Actions.toggleSpinner(false));
            }).catch(err => {
            const message = err.response.data;
                dispatch(Actions.setModalData({type: 'error', content: message.data}));
                modalShow(true);
                dispatch(Actions.toggleSpinner(false));
        });
    };

    return (
        <>
            <Table variant={'dark'} striped bordered hover width='auto'>
                <thead>
                {renderTableHeaders()}
                </thead>
                <tbody>
                {renderTableContent()}
                </tbody>
            </Table>
        </>
    )
}

function HeaderRow({fields}) {
    return (
        <tr>
            {fields.map((f) => {
                return (
                    <th className='orders__table-th' key={f}>{window.L10N.get(`orders.table.${f}`)}</th>
                )
            })}
        </tr>
    );
}

function BodyRow({data, handleApprove, handleDecline, handleUpload, changeHandler, fileInput, btnRefs, idx}) {
    let fields = [];
    for (let key in data.fields) {
        fields.push({key: key, value: data.fields[key]});
    }
    return (
        <tr>
            <td>{data.id}</td>
            <td style={{whiteSpace: 'nowrap'}}>{data.title}</td>
            <td>{data.status}</td>
            <td style={{whiteSpace: 'nowrap'}}>{data.dateOrdered}</td>
            <td>
                <div style={{display: "flex", flexWrap: "wrap", justifyContent: "flex-start", overflowX: 'hidden'}}>{fields.map((f, idx) => <div key={idx}
                                                                                                                                                 style={{marginRight: '5px'}}>
                    {f.key}: {f.value};</div>)}</div>
            </td>
            <td>
                <div className='btns-container'>
                    <input
                        multiple={false}
                        accept={'image/*'}
                        id={data.id}
                        ref={fileInput}
                        type="file"
                        name='file'
                        style={{display: "none"}}
                        onChange={changeHandler}/>
                    <Button data-id={data.id} variant={"info"} onClick={handleApprove}>{window.L10N.get('orders.table.approve')}</Button>
                    <Button data-id={data.id} variant={"danger"} onClick={handleDecline}>{window.L10N.get('orders.table.decline')}</Button>
                    <Button data-id={data.id} variant={'outline-info'} ref={ref => btnRefs.current[idx] = ref}
                            onClick={handleUpload}>{window.L10N.get('orders.table.upload')}</Button>
                </div>
            </td>
        </tr>
    )
}
