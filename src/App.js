import './css/App.css';
import Header from "./Header";
import Orders from "./orders";
import {useEffect, useState} from "react";
import {useSelector} from "react-redux";
import {Spinner} from "react-bootstrap";

function App({L10NLoaded}) {
    const {locale, showSpinner} = useSelector(state => state.reducer);

    useEffect(() => {

    });


    return (
        <div className="App">
            {L10NLoaded ? (
                <>
                    <Header/>
                    <div className="wrapper">
                        <Orders/>
                    </div>
                </>
            ) : null}
            {showSpinner !== undefined && showSpinner ?
                <div id='spinner-bg'><Spinner variant={"info"} style={{width: '10rem', height: '10rem'}} animation={'border'}/></div> : null}
        </div>
    );
}

export default App;
