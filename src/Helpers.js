import config from '../src/config.json';
import axios from "axios";

const Cookies = require('cookies-js');
let cache = {};

const getLang = () => {
    const languages = ['en', 'ru']
    const lang = navigator.language || navigator.browserLanguage || ((navigator.languages && navigator.languages.length) || ["en"])[0];

    return languages.find(l => lang.indexOf(l) !== -1) || languages[0];
};

export default class L10N {

    static load(locale) {
        return new Promise((resolve, reject) => {
            axios.get(`${config.labelsURL}${locale || getLang()}.json`)
                .then((labels) => {
                    cache = labels.data;
                    resolve(cache);
                })
                .catch(reject);
        });
    }

    static resolve(path, obj) {
        return path.split('.').reduce((prev, curr) => {
            if (prev) {
                return prev[curr];
            }
            return undefined;
        }, obj);
    }

    static get(key) {
        return L10N.getUnescaped(key);
    }

    static getUnescaped(key) {
        const val = L10N.resolve(key, cache);
        return val === undefined ? `{{${key}}}` : val;
    }
}

export function createAxiosInstance() {
    const token = Cookies.get('authToken');
    if (token) {
        window.axios = axios.create({
            baseURL: `${config[process.env.NODE_ENV].URL}`,
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${token}`
            }
        });
        return;
    }
    window.axios = axios.create({
        baseURL: `${config[process.env.NODE_ENV].URL}`,
        headers: {
            "Content-Type": "application/json",
        }
    });
}

window.L10N = L10N;
