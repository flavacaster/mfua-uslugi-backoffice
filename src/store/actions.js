import Type from './types';

export const setLocale = (locale) => ({type:Type.SET_LOCALE, payload:locale});
export const toggleSpinner = (boolean) => ({type:Type.TOGGLE_SPINNER, payload:boolean});
export const setModalData = (obj) => ({type:Type.SET_MODAL_DATA, payload:obj});
export const getStatusEnums = () => ({type:Type.GET_ENUMS});

