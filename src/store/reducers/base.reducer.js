import Type from "../types";

const initialState = {
    locale: 'ru-RU',
    showSpinner: false,
    statusEnums: {
        1: 'REQUESTED',
        2: 'READY',
        3: 'DECLINED',
    },
    modalData: {},
}

export function reducer(state = initialState, action) {
    switch (action.type) {
        case Type.SET_LOCALE:
            return {
                ...state,
                locale: action.payload,
            }
        case Type.SET_TOKEN:
            return {
                ...state,
                token: action.payload,
            }
        case Type.SET_MODAL_DATA:
            return {
                ...state,
                modalData: action.payload,
            }

        case Type.TOGGLE_SPINNER:
            return {
                ...state,
                showSpinner: action.payload,
            };
        case Type.GET_ENUMS:
            return {
                ...state
            }
        default:
            return state;
    }
}
