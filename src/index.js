import React from 'react';
import ReactDOM from 'react-dom';
import './css/index.css';
import App from './App';
import {Provider} from "react-redux";
import {store} from '../src/store/index';
import './sass/index.scss';
import './sass/layout.scss';
import L10N, {createAxiosInstance} from './Helpers';
createAxiosInstance();

L10N.load()
    .then(() => {
        ReactDOM.render(
            <Provider store={store}>
                <React.StrictMode>
                    <App L10NLoaded={true} />
                </React.StrictMode>
            </Provider>,
            document.getElementById('root')
        );
    });
