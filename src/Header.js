import React from "react";

export default function Header() {

    return(
        <div className="header">
            <div className="header__wrapper">
                <div className="header__logo logo">{window.L10N.get('logo')}</div>
                <div className="header__info">{window.L10N.get('backOffice')}</div>
            </div>
        </div>
    )

}

